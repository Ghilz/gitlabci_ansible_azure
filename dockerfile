FROM ansible/ansible:ubuntu1604py3

RUN apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    sshpass \
    && \
    apt-get clean

RUN ln -s /usr/bin/python3 /usr/bin/python && \
    python --version

#RUN pip3 install pip --upgrade && \
RUN pip3 install 'ansible[azure]' && \
    pip3 install 'azure>=2.0.0' --upgrade

